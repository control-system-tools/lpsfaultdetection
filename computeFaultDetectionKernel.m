function [kernel, parameters] = computeFaultDetectionKernel(tau, sys, dim, differentialExpressions, optArgs)
%COMPUTEFAULTDETECTIONKERNEL Kernel equation solution with L2-based performance index
% [kernel, parameters] = h22PerformanceIndexKernel(tau, sys, dim, differentialExpressions, optArgs)
% computes a solution of the kernel equation, specified by sys, dim, differentialExpression. For the
% performance index the sensitivity measures
%	S_f = || m_f^T Wf ||_2^2
%	S_d = || m_d^T Wd ||_2^2
% are used. The performance index is J = S_d / S_f. This is minimized under the additional constraint
% S_f = 1.
% From the input arguments, tau must be the time-domain on which the kernel is defined. With sys,
% the original system is specified. It must be a struct with the fields A, B, C, D, E1, E2, G1, G2,
% Gode, Gbounded, J
% With "dim", the dimensions of the matrices are specified. This structure must have the fields x,
% y, u, f, d, v, q. Where d is again a structure with the fields d, ode and bounded.
% The parameter differentialExpressions is a structure with the fields Q, N, M, Mf, Md.
% The optional arguments can be "nu" for the number of coefficients for the reference trajectory
% planning. "basis" for type of basis functions, it can be "sinusoidal or "polynomial". "mue0" the
% polynomial bump function. "Wf" and "Wd" as wighting functions for the sensitivity measure. 
%
%% H2 / H2 - performance index: J* = max || mf ||2 / || md ||2
arguments
	% time domain on which the kernel equation is defined as quantity.Domain object
	tau (1,1) quantity.Domain,
	% structure to specify the faulty system. It must have the fields A, B, C, D, E1, E2, G1, G2,
	% Gode, Gbounded, J
	sys struct {mustBeSysStruct},
	% structure with the dimensions of the original system. It must have the fields x, y, u, f, d,
	% v
	dim struct {mustBeDimStruct},
	% structure with the differential parameterizations for the kernel equation. It must have the
	% fields:  N, M, Mf, Md.
	differentialExpressions struct {mustBeDifferentialExpression}, 
	% Number of coefficients that should be used for the reference trajectory
	optArgs.nu (1,1) double = 2;
	% The type of basis functions that should be used for the computation of the reference
	% trajectory
	optArgs.basis (1,1) string = "polynomial",
	% The polynomial bump function used for the computation of the reference trajectory
	optArgs.mue0 (:,1) signals.BasicVariable = ones(dim.y,1) * signals.PolyBump(tau, "a", dim.x, ...
		"b", dim.x, "norm", true, "numDiff", dim.x);
	% Weighting matrix for the sensitivity measure of the fault
	optArgs.Wf double = eye( dim.f );
	% Weighting matrix for the sensitivity measure of the disturbance
	optArgs.Wd double = eye( dim.d );
	% 
	optArgs.SfDesired (1,1) double = 1;
end

% create the basis functions for the reference trajectory planning:
if strcmp(optArgs.basis, "sinusoidal")
	basis = signals.Sinusoidal.fourierBasis( tau, "numDiff", ...
		max( [optArgs.mue0.highestDerivative] ), "order", optArgs.nu);
elseif strcmp(optArgs.basis, "polynomial")
	basis = signals.Monomial.taylorPolynomialBasis( tau, "numDiff", ...
		max( [optArgs.mue0.highestDerivative] ), "order", optArgs.nu);
else
	error("Not yet implemented");
end

% compute the basis functions for the fault filter:
% the components of the reference trajectory are given by
% phi(i) = mue(i) * basis(i) * a(i)
%	respectively:
% phi = Theta * a
%
% Theta = [mue0(1) * basis(1),	0,	0, ...
%		[   0,   mue0(1) * basis(2),		0,		0, ....
%		[   0,		0,		mue0(1) * basis(3), ....
%
Theta = kron( eye(dim.y), basis');
for i = 1:dim.y
	Theta(i,:) = optArgs.mue0(i) * Theta(i,:);
end
% compute the basis functions for the integral kernel
%	m_f(tau) = xi(tau) * a
xi = differentialExpressions.Mf.applyTo( Theta );
% compute the symmetric matrix for the sensitivity measure:
%	S_f = a' Xi a
Xi = int( xi.' * optArgs.Wf * optArgs.Wf.' * xi );
% compute the basis functions for the disturbance filter:
% m_d(tau) = upsilon(tau) * a
upsilon = differentialExpressions.Md.applyTo( Theta );
% compute the matrix for the sensitivity measure:
%	S_d = a' * Upsilon * a
Upsilon = int( upsilon.' * optArgs.Wd * optArgs.Wd.' * upsilon );

% performance index:
%	J = S_d / S_f 
% 
% Optimiering: min J so that S_f = 1
%	L = a' Upsilon a - lmbda( a' Xi a - 1 )
%  \nablda L = 2 Upsilon a - lmbda Xi a
%
%	=>  Upsilon a = lmbda Xi a
%         a' Xi a = 1
[V, Lmbda] = eig( Upsilon, Xi );
% verify that all eigen values are real and not negative
assert( isreal( Lmbda(:) ) && all( Lmbda(:) >= 0 ) )
% get the minimal eigenvalue
[~,i] = min( diag( Lmbda )	 );
% scale wMin so that ||mf || = 1
% compute the optimal solution for the coefficients a
wMin = V(:,i);
% scale the eigenvector so that S_f = 1
aStar = wMin / sqrt( wMin.' * Xi * wMin ) * sqrt( optArgs.SfDesired );
Sf = aStar.' * Xi * aStar;
Sd = aStar.' * Upsilon * aStar;
JStar = Sd / Sf;
% verify that S_f = 1 and JStar is non negative:
%assert( numeric.near( Sf, 1, 1e-3 ) )
assert( JStar >= 0 )

% compute the reference trajectory
phi = Theta * aStar;
phi.setName("\phi");

% compute the reference trajectories with the phi:
kernel.N = differentialExpressions.N.applyTo(phi, "resultName", "n");
kernel.M = differentialExpressions.M.applyTo(phi, "resultName", "m");
kernel.Mf = differentialExpressions.Mf.applyTo(phi, "resultName", "m_f");

kernel.Md = sys.G1.' * kernel.M + sys.G2.' * kernel.N;
kernel.Md.setName("m_d");

kernel.Mu = -sys.B.' * kernel.M;
kernel.Mu.setName("m_u");

% parameters.optResults = optMax;
% parameters.i = i;
parameters.phi = Theta;
parameters.nu = optArgs.nu;
parameters.J = JStar;
parameters.Sf = Sf;
parameters.Sd = Sd;
parameters.a = aStar;
parameters.phi = phi;
end

function mustBeSysStruct(s)
	if ~all(isfield(s, ["G1", "G2", "B"])) 
		throwAsCaller(MException( "kernelPerformanceIndex:mustBeSysStruct", "The argument sys must be a structure with the fields ..."));
	end
end

function mustBeDimStruct(dim)

	if ~all(isfield(dim, ["x", "f", "d"]))
		throwAsCaller(MException( "kernelPerformanceIndex:mustBeDimStruct", "The argument dim must be a structure with the fields x, v, f, d, d.bounded"));
	end
end

function mustBeDifferentialExpression(d)

	if ~all( isfield(d, ["Mf", "Md", "N", "M"]) )
		throwAsCaller(MException( "kernelPerformanceIndex:mustBeDifferentialExpression", ...
			"The argument differentialExpression must be a structure with the fields Mf, Md, N"));
	end

end
