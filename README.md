# Matlab code for the fault detection of lumped-parameter systems using integral transformations and trajectory planning methods
This repository provides matlab code for the fault detection for lumped-parameter systems using integral transformations and trajectory planning methods. The faulty system of Example 12.3 from [1] (without taking the feed-through into account) is implemented in `example12_3_Ding.m`. The fault detection approach using integral transformations and trajectory planning is implemented and compared to the observer based fault detection approach in Example 12.3 from [1]. 
This is the companion implementation to the paper "Fault Detection for Lumped-Parameter LTI Systems using Integral Transformations and Trajectory Planning Methods" submitted to SysTol'21.

## Prerequisites
The matlab code is implemented and tested in Matlab 2020a.

The implementation is based on the conI - Matlab Library https://gitlab.com/control-system-tools/coni, which must be included in the matlab path. It is included as submodule in the git repository.

## How to use
* `init.m` adds the `coni` path and sets some further useful options.
* `computeFaultDetectionKernel.m` is a function to compute the fault detection kernel based on the optimization of a performance index. 
* `computeFirFilter.m` creates the FIR filter for the computed detection kernels, which is based on a time discretization of the integral expressions.
* `firFaultDetection.m` a function to evaluate the fault detection filter. It contains an algorithm to detect the faults, based on the residual signal in an online fashion way.
* `plotKernel.m` helper function to visualize the computed kernel.

### Coding Guidelines
For general hints, see [the related wiki-page.](https://gitlab.com/control-system-tools/coni/-/wikis/Coding-guidelines)

### Contributing
We are looking forward to your feedback, criticism and contributions. Feel free contact us and don't hesitate to commit changes. We will review all commits to ensure quality.

## Questions & Contact
Feel free to contact [Ferdinand](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/fischer-ferdinand-m-sc/) in case of suggestions or questions.

## License
GNU General Public License v3.0 (GPL), see [Licence-file](https://gitlab.com/control-system-tools/lpsfaultdetection/-/blob/master/COPYING).

# REFERENCES
[1] S. X. Ding. Model-based Fault Diagnosis Techniques: Design Schemes, Algorithms, and Tools. Springer–Verlag, Berlin Heidelberg, 2008.
