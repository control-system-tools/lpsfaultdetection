function [r, data, figureHandle] = firFaultDetection(fir, y, u, optArgs)
% FAULTDIAGNOSIS Perform the fault diagnosis
%	[tHat, fHat, data] = faultDiagnosis(obj, filters, y, u, optArgs) will apply the fault
%	diagnosis filter "filters" to the measurement y and input u. The filters must be defined
%	in a struct with at least the fields: "N", "Mu" and "dt". Additional options can be specified
%	with "optArgs". These are:
%	"f": the fault that should be detected. If the fault is also passed to the function,
%		the filter r_f(t) = < Mf, f(t) > is also evaluated. This is helpful for debugging.
%	"d": the disturbance used for the simulation. TODO This property is not used so far.
%		This is also required to verify the term r_f(t) = < Mf, f(t) > + < Md, d(t) >, which
%		can be useful for debugging pruposes.
%	"plot": defines if the result should directly be plotted
%	"t": the time domain as quantity.EquidistantDomain with the same discretization as used
%	for the generation of the fir filters.
arguments
	fir
	y (:,1) quantity.Discrete;
	u (:,1) quantity.Discrete;
	optArgs.f (:,1) quantity.Discrete;
	optArgs.occurrence (:,1) double;
	optArgs.d (:,1) quantity.Discrete;
	optArgs.plot (1,1) logical = true;
	optArgs.t (:,1) quantity.EquidistantDomain;
	optArgs.plotThresholds (1,1) logical = false;
	optArgs.figId (1,1) double;
	optArgs.plotFault (1,1) logical = true,
	optArgs.d_bounded (:,1) quantity.Discrete;
end

%% input parsing:
if isfield(optArgs, "t")
	t = optArgs.t;
else
	t = y.domain;
	
	if ~isa(t, "quantity.EquidistantDomain")
		t = quantity.EquidistantDomain( t );
	end
end

% assert that the output and input filter is available
assert( isfield(fir, "N") && isfield(fir, "Mu") && isfield(fir, "dt"));

% assert that time discretization of t and the created filters is equal.
assert( numeric.near( fir.dt, t.stepSize), "Step size of the fir filters and the simulation time must be equal" );

%% evaluation of the fault diagnosis filters:
ry = signals.fir.mFilter(fir.N, y.on(t), "t", t);
ru = signals.fir.mFilter(fir.Mu, u.on(t), "t", t);

r = ry + ru;
r.setName("fHat");

%% plot the fault and the threshold
if optArgs.plot
	
	labels = {};
	
	if ~isfield( optArgs, "figId")
		figureHandle = figure();
		optArgs.figId = figureHandle.Number;
	elseif ishandle( optArgs.figId )
		set(groot, 'CurrentFigure', optArgs.figId );
		figureHandle = gcf();
		%figureHandle = figure( optArgs.figId );
	else
		figureHandle = figure( optArgs.figId );
	end
	clf;
	
	if isfield( optArgs, "f")
		
		if optArgs.plotFault
			plot( optArgs.f, "figureId", 0, "smash", true, "hold", true, "LineStyle", "-", "Color", "k");
			labels = {"$f(t)$"};
		end
		
		% compute ff = < M_f, f(t) >
		rf = signals.fir.mFilter( fir.Mf, optArgs.f.on(t) );
		rf = quantity.Discrete(rf, t);
		
	end
	
	if isfield( optArgs, "d_bounded")
		
		rd = signals.fir.mFilter( fir.Md, optArgs.d_bounded.on(t) );
		rd = quantity.Discrete(rd, t);
		
	end
	
	plot( r, "fig", -1, "hold", true, "Color", "red", "LineWidth", 2 );
	labels = {labels{:}, "$r(t)$"};
	
	if optArgs.plotThresholds
		
		% plot the threshold
		fBoundUpper = fir.threshold(:) * quantity.Discrete.ones(1, t);
		fBoundUpper.setName("fBoundUpper");
		fBoundLower = - fBoundUpper;
		fBoundLower.setName("fBoundLower");
		
		plot( fBoundUpper, "fig", -1, "hold", true, "Color", "green", ...
			"LineWidth", 1, "LineStyle", ":" );
		plot( fBoundLower, "fig", -1, "hold", true, "Color", "green", ...
			"LineWidth", 1, "LineStyle", ":" );
		labels = {labels{:}, "$f_B^+$", "$f_B^-$"};
		
		% plot the estimation bounds:
		if isfield( optArgs, "f")
			fHatUpper = rf + fBoundUpper;
			fHatLower = rf - fBoundUpper;
		else
			fHatUpper = r + fBoundUpper;
			fHatLower = r - fBoundUpper;
		end
		
		fHatUpper.setName("fHatUpper");
		fHatLower.setName("fHatLower");
		
		hold on;
		plot( fHatUpper,  "fig", -1, "hold", true, "Color", "green", ...
			"LineWidth", 1, "LineStyle", "--" );
		plot( fHatLower,  "fig", -1, "hold", true, "Color", "green", ...
			"LineWidth", 1, "LineStyle", "--" );
	end
	
	% plot initial detection window
	for i = 1:size(r,1)
		h = subplot(size(r,1), 1, i);
		ylim = h.YLim;
		a = area([0, fir.T], ...
			ylim(2) * ones(2,1), ylim(1));
		a.FaceColor = [0 0.4470 0.7410];
		a.FaceAlpha = 0.2;
		a.LineStyle = 'none';
	end
	
	
	% if available draw detection windows:
	if isfield(optArgs, "occurrence")
		for i = 1:length(optArgs.occurrence)
			for j = 1:size(r,1)
				h = subplot(size(r,1), 1, j);
				ylim = h.YLim;
				a = area([optArgs.occurrence(i), optArgs.occurrence(i)+fir.T], ...
					ylim(2) * ones(2,1), ylim(1));
				a.FaceColor = [0 0.4470 0.7410];
				a.FaceAlpha = 0.2;
				a.LineStyle = 'none';
			end
		end
	end
end

%% perform the fault detection
% Although, the fault detection residual is computed in one step, the fault detection is done as it
% must be online by iterating over all time steps:
assert(all(size(r) == 1))
% get the numerical result of the residual:
rData = r.on(t);
% initialize a flag state that indicates whether a fault is detected or not
faulty = zeros(size(rData));
% get the first index from which the fault detection should start
Tidx = size(fir.N,1);
nFaults = 0;
h = subplot(1,1,1);
detection = struct();
for i = Tidx+1:length(rData)
	% actual residual value:
	rt = rData(i,:);
	% verify if the fault detection condition is true:
	if abs(rt) > fir.threshold
		% set the flag variable that indicates the presence of a fault to true
		faulty(i) = 1;
		% if for a detection window length no fault was detected, the exceed of the
		% threshold indicates the occurrence of a fault:
		if all( faulty(i-Tidx:i-1) == 0)
			% raise the fault counter
			nFaults = nFaults + 1;
			% note the fault detection index
			detection(nFaults).idx = i;
			% note the fault detection time
			detection(nFaults).start = t.grid(i);
			% mark the detection time in the plot
			plot( detection(nFaults).start, rData(detection(nFaults).idx), 'x', 'MarkerSize', 10, 'LineWidth', 2);
			% for the plot a end value of the detected fault is required. At this time online inf
			% can be assumed.
			detection(nFaults).windowEnd = inf;
		end
	else
		% if the fault detection condition is not true, decide if there was a fault or not.
		% Since it requries at least a detection window length until the residual value stays
		% secured under the threshold value, check if for the last detection window, no fault was
		% detected.
		if all( faulty(i-Tidx:i-1) == 0 ) && nFaults > 0 && detection(nFaults).windowEnd == inf
			% If no fault was detected for at least one detection window and there was a fault
			% detected before, it can be assumed to be not present any more:
			% Note the end of the fault occurrence
			detection(nFaults).windowEnd = t.grid(i);
			% Mark the faulty domain in the plot
			ylim = h.YLim;
			a = area([detection(nFaults).start, detection(nFaults).windowEnd], ylim(2) * ones(2,1), ylim(1));
			a.FaceColor = [0.8500 0.3250 0.0980];
			a.FaceAlpha = 0.2;
			a.LineStyle = 'none';
		end
	end
end
% if present, mark the last fault in the plot
if nFaults > 0 && detection(nFaults).windowEnd == inf
	detection(nFaults).windowEnd = t.grid(i);
	ylim = h.YLim;
	a = area([detection(nFaults).start, detection(nFaults).windowEnd], ylim(2) * ones(2,1), ylim(1));
	a.FaceColor = [0.8500 0.3250 0.0980];
	a.FaceAlpha = 0.2;
	a.LineStyle = 'none';
end

% plot the reference solution for the fault estimation:
% This is useful to verify the fault detection filter:
if isfield( optArgs, "f" )
	
	if optArgs.plot
		plot( rf, "fig", -1, "hold", true, "LineStyle", "--", "Color", "b");
	end
	labels = {labels{:}, "$\tilde{f}(t)$" };
	
end

% plot the reference solution for the disturbance part of the residual:
% This is useful to verify the disturbance part of the residual
if isfield( optArgs, "d_bounded")
	if optArgs.plot
		plot( rd, "fig", -1, "hold", true, "LineStyle", "--", "Color", "m");
	end
end
% Add the legend to the plot
if optArgs.plot
	legend(labels, "Interpreter", "Latex")
end

% If required, create a structure with information about the fault detection
if nargout >= 2
	
	parameters = struct();
	
	for i = 1:size(r,1)
		paramters.("fb" + num2str(i)) = fir.threshold(i);
	end
	
	if nFaults > 0
		for j = 1:numel(detection)
			parameters.("detection" + num2str(j)) = detection(j).start;
			parameters.("detectionT" + num2str(j)) = detection(j).windowEnd;

			if isfield(detection(j), "No")
				parameters.("detectionNo" + num2str(j)) = detection(j).No;
				parameters.("faultDetection" + num2str(j)) = ...
					r(detection(j).No).at(detection(j).start);
			end
		end
	end
	data.parameters = parameters;
	
	if isfield( optArgs, "f")
		data.rf = rf;
	end
	
	if isfield( optArgs, "d_bounded")
		data.rd = rd;
	end
	
	if optArgs.plotThresholds
		data.boundUpper = fBoundUpper;
		data.boundLower = fBoundLower;
		data.fHatUpper = fHatUpper;
		data.fHatLower = fHatLower;
	end
	
end
end % function faultDiagnosis