% all figure should be docked:
set(0,'DefaultFigureWindowStyle','docked');

% add the coni toolbox
addpath coni
S = quantity.Settings.instance();
S.plot.dock = true;
S.plot.name2title = false;
S.plot.name2axis = true;
S.plot.grabFocus = false;

% set the default matlab text interpreter to latex:
set(0,'defaultTextInterpreter','latex');