function filters = computeFirFilter(I, modulatingFunctions, optArgs)
%FIRFIRSTORDERHOLD Summary of this function goes here
%   Detailed explanation goes here
arguments
	I (1,1) quantity.EquidistantDomain;
	modulatingFunctions
	optArgs.bounds (:,1) double = ones( size( modulatingFunctions.Md, 2 ), 1);
	optArgs.boundsL1 (:,1) double = ones( size( modulatingFunctions.Md, 2 ), 1);
	optArgs.type (1,1) string = "";
	optArgs.thresholdType (1,1) string = "l1"; % can be l1 or l2
	optArgs.firApproximation = "firstOrderHold"
end

switch optArgs.firApproximation

	case "firstOrderHold"
		filters.N = signals.fir.firstOrderHold(modulatingFunctions.N.', I, 'flip', false);
		filters.Mu = signals.fir.firstOrderHold(modulatingFunctions.Mu.', I, 'flip', false);
		filters.Mf = signals.fir.firstOrderHold(modulatingFunctions.Mf.', I, 'flip', false);
		filters.Md = signals.fir.firstOrderHold(modulatingFunctions.Md.', I, 'flip', false);
		
	case "midPoint"
		filters.N = signals.fir.midpoint(modulatingFunctions.N.', I, 'flip', false);
		filters.Mu = signals.fir.midpoint(modulatingFunctions.Mu.', I, 'flip', false);
		filters.Mf = signals.fir.midpoint(modulatingFunctions.Mf.', I, 'flip', false);
		filters.Md = signals.fir.midpoint(modulatingFunctions.Md.', I, 'flip', false);
			
	case "trapez"
		filters.N = signals.fir.trapez(modulatingFunctions.N.', I, 'flip', false);
		filters.Mu = signals.fir.trapez(modulatingFunctions.Mu.', I, 'flip', false);
		filters.Mf = signals.fir.trapez(modulatingFunctions.Mf.', I, 'flip', false);
		filters.Md = signals.fir.trapez(modulatingFunctions.Md.', I, 'flip', false);
					
		
	otherwise
		error("Unknown fir approximation scheme");
	
end

if optArgs.thresholdType == "l1"
	filters.threshold(:,1) = int( abs( modulatingFunctions.Md.' ), I) * optArgs.bounds;	
elseif optArgs.thresholdType == "l2"
	filters.threshold(:,1) = ...
		sqrt( int( modulatingFunctions.Md.^2 ) ).' * sqrt(  optArgs.bounds );
	%l2norm( modulatingFunctions.Md, "weight", diag(optArgs.bounds) );
elseif optArgs.thresholdType == "l1l2"
	filters.thresholdL2 = ...
		sqrt( int( modulatingFunctions.Md.^2 ) ).' * sqrt(  optArgs.bounds );
	filters.thresholdL1 = sum( int( abs( modulatingFunctions.Md.' ), I) * optArgs.bounds);
else
	error("Unknown")
end

filters.dt = I.stepSize;
filters.T = I.upper;
filters.type = optArgs.type;
end

