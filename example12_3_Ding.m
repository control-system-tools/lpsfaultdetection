%% EXAMPLE
% This is the companion implementation to the paper 
%	Fault Detection for Lumped-Parameter LTI Systems using Integral Transformations and Trajectory
%	Planning Methods
% submitted to SysTol'21. It implements the Example 12.3 from [1] without taking the feed-through
% into account. In the first section the system and the required time domains are defined.
% Subsequently, the used input signals, including the control input, the disturbances and the faults
% are created. In Section 3, the kernel equation and its flatness based solution are implemented.
% This includes the fault detection and the plotting of the results. To compare these results with
% the observer-based approach from Example 12.3 in [1], the observer-based approach is implemented
% in Section 4 and the simulation results are plotted. The comparison based on moving horizon energy
% measure is presented in Section 5. The choice of T is motivated by the parameter study in Section
% 6 and in Section 7, the resutls are exported so that they can be used in the paper.
% Most of the implementation is based on the matlab toolbox [2];

% REFERENCES
% [1] S. X. Ding. Model-based Fault Diagnosis Techniques: Design Schemes, Algorithms, and Tools.
%		Springer–Verlag, Berlin Heidelberg, 2008.
% [2] F. Fischer, J. Gabriel, and S. Kerschbaum. coni - a Matlab toolbox
%		facilitating the solution of control problems, v1.1. Zenodo, 2021. 
%		http://doi.org/10.5281/zenodo.4555225

init() % include the conI path and set some further useful properties.

% Set these flags to true to include the parameter study for T and/or export the results to *.dat
% files
flag.perfomanceIndexStudy = true;
flag.export = true;

%% 1. faulty lti system
A = [-3 -0.5 0.8 1
      1  -4  0  -1
      2  -3  -1 0.5
      0   1  -2 0];
B = [3;
	 2;
	 1;
	 1];
C = [1 -0.25 -1 0
    0     1   0 1];
% in contrast to the Example 12.3 in [1], the feed-through is not taken into account. 
D = [0; 0];

G1 = [0.5 -1 1
      0.8 0.5 0
      0   -1  1
      0.2  0  0.5];
E1 = [-1 0; 
	  -0.5 1; 
	   0.2 1; 
	   -1 0];
G2 = [0.5 1 0;
        1 0 1];
E2 = [1 0;
      0 1.5];

% save the system matrices in a struct because they are required later in this format
systemMatrices.A = A;
systemMatrices.B = B;
systemMatrices.C = C;
systemMatrices.D = D;
systemMatrices.E1 = E1;
systemMatrices.E2 = E2;
systemMatrices.G1 = G1;
systemMatrices.G2 = G2;
% to simplify the access to the system dimesnions the following struct is created
dim.x = size(A,1);
dim.u = size(B,2);
dim.y = size(C,1);
dim.f = size(E1,2);
dim.d = size(G1,2);
% create a state space object to simulate the faulty system
sys = ss(A, [B, G1 E1], C, [D, G2, E2]);
% initialization of the time domains. The domain "t" is the time-domain for the simulation and the
% time "tau" is the time-domain for the fault detection kernels. The step-size 0.5 is chosen because
% it is recommended by the lsim solver.
t = quantity.EquidistantDomain("t", 0, 1000, "stepSize", 0.5);
T = 50;
tau = quantity.EquidistantDomain("tau", 0, T, "stepSize", t.stepSize);

%% 2. Define the inputs for the simulation example:
% In this section the inputs for the simulation are constructed. 

% Define the disturbances: d(1) is a sinusoidal disturbance with frequency 0.1 and amplitude 0.1
inputs.d.bounded(1,1) = quantity.Discrete( 0.1 * sin( 0.1 * t.grid) , t, "name", "d");

% the disturbance d(2) is a pseudo-random continuous signal taking values randomly from a uniform
% distribution between +- 0.1
% This continuous pseudo-random signal can be easily created with the signals.PseudoRandomSignal
% from the conI matlab toolbox [2].
inputs.d.l1bounds = .1;
inputs.d.bounded(2,1) = signals.PseudoRandomSignal(t, 'seedt', 2, 'seedf', 2, 'fmin', -inputs.d.l1bounds(1), ...
	'fmax', inputs.d.l1bounds(1), 'name', 'd', "normalize", false, "steps", 100, "filter", "gevrey");

% the disturbance d(3) is a chirp between 0.02 Hz and 0.06 Hz with an amplitude of 0.1
d3 = chirp( 0:t.stepSize:(t.upper / 4), 0.02, (t.upper / 4), 0.06);
d3 = d3(1:end-1);
inputs.d.bounded(3,1) = .1 * quantity.Discrete( [d3 flip(d3) d3 flip(d3) 1]', t);
inputs.d.bounded.setName("d");

% compute the moving horizon L2-bound for the disturbance:
%	int_t-T^t d_i^2(tau) dtau = delta_i
% This is easily achieved with a moving average filter applied to the square of the disturbance:
W = quantity.Discrete.eye(dim.d, tau);
w = signals.fir.midpoint( W, tau );
L2filteredDisturbance = signals.fir.mFilter(w, inputs.d.bounded.^2);
inputs.d.l2Bounds = max(L2filteredDisturbance);

% the input signal is a pseudo-random continuous signal taking values randomly from a uniform
% distribution between +- 5
inputs.u = signals.PseudoRandomSignal(t, "steps", 9, "seedt", 2, "seedf", 10, "fmin", -5, ...
	"fmax", 5, "filter", "gevrey", "name", "u");

% Three fault scenarios are considered. 
% I:   f1(t) = 0.4 for 200 < t < 300
% II:  f2(t) = 0.6 for 500 < t < 600
% III: f1(t) = f1_(t) for t > 800 and f2(t) = f2_(t) for t>900 where f1_ and f2_ are pseudo-random
% continuous signals taking values randomly from a uniform distribution between 0 and 0.2
occurrences = [200, 500, 800, 900];
disappears = [300, 600, t.upper, t.upper];

fI_II(1,1) = ...
	quantity.Discrete( 0.4 * (t.grid >= occurrences(1) & t.grid <= disappears(1)), t, "name", "f1");
fI_II(2,1) = ...
	quantity.Discrete( 0.6 * (t.grid >= occurrences(2) & t.grid <= disappears(2)), t, "name", "f2");

% for the scenario III:, a piecewise constant function is defined which is multiplied with the
% pseudo random signals.
fIII(1,1) = quantity.Discrete( t.grid >= occurrences(3), t, "name", "III1");
fIII(2,1) = quantity.Discrete( t.grid >= occurrences(4), t, "name", "III2");

% pseudo-random signal generators for f1_ and f2_
f2(1,1) = signals.PseudoRandomSignal(t, 'seedt', 1, 'seedf', 1, 'fmin', 0, ...
	'fmax', 0.2, 'name', 'd', "normalize", false, "steps", 23, "filter", "gevrey");
f2(2,1) = signals.PseudoRandomSignal(t, 'seedt', 3, 'seedf', 7, 'fmin', 0, ...
	'fmax', 0.2, 'name', 'd', "normalize", false, "steps", 28, "filter", "gevrey");

inputs.f = fI_II + fIII .* f2;
inputs.f.setName("f");

% do the simulation with the created input signals:
sim.t = t;
[sim.y, ~, sim.x] = lsim(sys, [inputs.u.on(sim.t), ...
	inputs.d.bounded.on(sim.t), inputs.f.on(sim.t)], sim.t.grid);
% convert the simulation data to quantity.Discrete objects and reproduce the input signals.
sim.y = quantity.Discrete(sim.y, sim.t, "name", "y");
sim.x = quantity.Discrete(sim.x, sim.t, "name", "x");
sim.u = inputs.u.subs("t", sim.t);
sim.f = inputs.f.subs("t", sim.t);
sim.d = inputs.d.bounded.subs("t", sim.t);

% plot simulation result
figure(1); clf;
plotArgs = {"figureId", 0, "hold", true, "smash", true};
subplot(3,3,1);
plot( sim.u, plotArgs{:});

subplot(3,3,2);
plot( sim.d, plotArgs{:});

subplot(3,3,3); 
plot( sim.f, plotArgs{:});

subplot(3,3,[4,5,6]);
plot( sim.x, plotArgs{:});

subplot(3,3,[7,8,9]);
plot( sim.y, plotArgs{:});

suptitle("Simulation");

%% 3. kernel equation and flatness based solution
% kernel system:
kernel = ss( A.', ...
			 C.', ...
			 E1.', E2.');
		  		  
% verify that the kernel system is controllable
assert( rank( ctrb( kernel ) ) == dim.x, "Kernel equation must be controllable.");

% flatness based solution for the kernel equations:
assert( rank( kernel.B ) == dim.y ) % verify that there are no linear dependent inputs:

% compute the flat out and the differential parametrization for the kernel equations:
[flat.Cf, flat.kappa, flat.dp] = stateSpace.flatOutput(kernel.A, kernel.B);

flat.diffParam.N = flat.dp.U;
flat.diffParam.M = flat.dp.X;
flat.diffParam.Mf = E1.' * flat.diffParam.M + E2.' * flat.diffParam.N;
flat.diffParam.Md = G1.' * flat.diffParam.M + G2.' * flat.diffParam.N;

% compute the polynomial bump functions for the reference trajectories:
flat.mue = signals.PolyBump.empty;
for i = 1:dim.y
	flat.mue(i,1) = signals.PolyBump(tau, "a", flat.kappa(i), "b", flat.kappa(i), "norm", true, "numDiff", flat.kappa(i));
end

% solve the kernel equations with a H_2^2 optimal performance index:
figureIdOffSet = 0;	% use this to produce plots for the kernels and the detection result with different figure numbers.


% The parameter "nu" sets the number of degrees of freedom for each flat output. This is chosen, so
% that no relevant change occurrs if a higher degree is taken. Note, the described monomes tau^i as
% basis function for the degrees of freedom are only taken for the sake of brevity. However, they
% lead very fast to numerical problems. Basis functions based on sinusoidals as used in fourier
% series are much more reliable. To examine the difference, the optional argument "basis" can be set
% to "sinusoidal".
% the parameter SfDesired is chosen so that the influence of the disturbances on the residual in the
% fault free case is set to a similar value as obtained with the observer based results. This is
% only a scaling factor for the fault detection results.
[flat.kernel, flat.args] = computeFaultDetectionKernel(tau, systemMatrices, ...
	dim, flat.diffParam, "nu", 9, "basis", "polynomial", "mue0", flat.mue, "SfDesired", 0.5 );

plotKernel( flat.kernel, flat.args.phi, "figureID", 2 + figureIdOffSet);
titleText = sprintf( "$L_2$-Norm Permfomance Index \n T = %i, $\\nu$ = %i and $J^* = \\frac{||m_d^T W_d ||_2^2}{||m_f^T W_f ||_2^2} = %g $" , ...
	T, flat.args.nu+1, flat.args.J);
suptitle( titleText );

% do the time discretization. Different methods for the approximation of the integral expression
% can be used. Due to brevity, the reapezoidal rule is used in the paper.
flat.fir = computeFirFilter(tau, flat.kernel, "type", "detection", ...
	"bounds", inputs.d.l2Bounds, "thresholdType", "l2", "firApproximation", "trapez");

% performe the fault detection:
[flat.r, flat.data] = firFaultDetection(flat.fir, sim.y, ...
	sim.u, "f", sim.f, "plotFault", false, ...
	"d_bounded", sim.d, ...
	"t", t, ...
	"figId", 3 +figureIdOffSet, "occurrence", occurrences, "plotThresholds", true);
suptitle( titleText );

%% 4. observer based fault detection
% in this section the results from Example 12.3. in [1] are implemented. There two different
% observers for the residual generator are proposed. In the following only the second observer is
% considered since it is the one with the better ratio between sensitivity to faults and
% disturbances, which is one of the points where the integral transformation based method and the
% observer based approach are compared in.
L1 = [-0.9735 0.1323
    -0.5639  0.5791
    -0.2118  0.6198
    -0.4837  0.4439];
V1 = [1 0
      0 0.6667];
  
L2 = [-1.0072 1.0029
       0.6343 0.2393
       -1.1660 0.7751
       -0.0563 0.3878];
V2 = [0.9333 -0.1333
      -0.1333 0.7333];

sys_new1 = ss(A-L1*C, [B-L1*D, L1], C, [D,zeros(2,2)]);
sys_new2 = ss(A-L2*C, [B-L2*D,L2], C, [D,zeros(2,2)]);

obs1.y = lsim(sys_new1, [ inputs.u.on(sim.t), sim.y.on()], sim.t.grid);
obs2.y = lsim(sys_new2, [ inputs.u.on(sim.t), sim.y.on()], sim.t.grid);

r1 = V1 * quantity.Discrete( sim.y.on() - obs1.y, sim.t );
r2 = V2 * quantity.Discrete( sim.y.on() - obs2.y, sim.t );

% threshold computation
Gyd = tf( ss( A, G1, C, G2 ) );
Mu = tf( ss( A - L1 * C, L1, -C, eye(dim.y)) );
Nd = V1 * tf( ss( A - L1*C, G1 - L1*G2, C, G2) ) ;
hinfnorm(Nd);
%eye(dim.y) - C * ( tf("s") * eye(dim.x) - A + L1 * C)^(-1) * L1;

% overall system:
residualGenerator1.Gd = ss( [A zeros(dim.x); L1*C A- L1*C], [G1; L1*G2], V1*[C, -C], V1*G2 );
residualGenerator1.Gf = ss( [A zeros(dim.x); L1*C A- L1*C], [E1; L1*E2], V1*[C, -C], V1*E2 );
residualGenerator1.threshold = hinfnorm(residualGenerator1.Gd) * l2norm( inputs.d.bounded );

V11 = V1(1,:);
residualGenerator1.Gd1 = ss( [A zeros(dim.x); L1*C A- L1*C], [G1; L1*G2], V11*[C, -C], V11*G2 );
residualGenerator1.threshold1 = hinfnorm(residualGenerator1.Gd1); % * l2norm( inputs.d.bounded );

V12 = V1(2,:);
residualGenerator1.Gd2 = ss( [A zeros(dim.x); L1*C A- L1*C], [G1; L1*G2], V12*[C, -C], V12*G2 );
residualGenerator1.threshold2 = hinfnorm(residualGenerator1.Gd2); % * l2norm( inputs.d.bounded );

figure(4); clf;
subplot(211);
plot( r2(1), plotArgs{:});
xlabel("t")
ylabel("r")

subplot(212);
plot( r2(2), plotArgs{:});
xlabel("t")
ylabel("r")
suptitle(sprintf( "Reference results from Observer-Based Fault Detection \n $H_\\infty$ Performance Index") );

%% 5. compare the observer based solution with the integral-transformation based approach:
% compute average moving hoirzion L2-gains:
av.t = quantity.EquidistantDomain("tau", 0, 50, "stepSize", t.stepSize);
av.W = quantity.Discrete.eye(1, av.t); % 1/av.t.upper * 
av.fir = signals.fir.midpoint( av.W, tau );
flat.avR = signals.fir.mFilter( av.fir, flat.r.^2 );

obs.avR1 = signals.fir.mFilter( av.fir, r2(1).^2);
obs.avR2 = signals.fir.mFilter( av.fir, r2(2).^2);

figure(5); clf; 
semilogy( t.grid, flat.avR.on()); hold on;
semilogy( t.grid, obs.avR1.on());
semilogy( t.grid, obs.avR2.on());
legend(["||r||_{2,I}^2", "||r_{obs}^1||_{2,I}^2", "||r_{obs}^2||_{2,I}^2"]);
ylim([1e-2, 500]);
xlim([0, t.upper])

h = gca;
for i = 1:numel( occurrences )
	a = area([occurrences(i), disappears(i) + T], ...
		h.YLim(2) * ones(2,1), h.YLim(1));
	a.FaceColor = [0 0.4470 0.7410];
	a.FaceAlpha = 0.2;
	a.LineStyle = 'none';
end
title(sprintf( "Sliding energy level of the residuals \n T = %i, $\\nu$ = %i and $J^* = \\frac{||m_d^T W_d ||_2^2}{||m_f^T W_f ||_2^2} = %g $" , ...
	T, flat.args.nu+1, flat.args.J))

%% 6. Parameterstudie für T
if flag.perfomanceIndexStudy
	Trange = [linspace(9, 20, 5), logspace(log10(21), log10(100), 10)]; %logspace( log10( 0.1 ), log10(100), 60);
	l2T = struct();
	for k = 1:length(Trange)
		fprintf("Compute the kernels for T = %i ", Trange(k));

		tauRange = quantity.EquidistantDomain("tau", 0, Trange(k), "stepNumber", 1e3);
		study.t = quantity.EquidistantDomain("t", t.lower, t.upper, "stepSize", tauRange.stepSize);
		simT.dBounded = sim.d.subs("t", study.t);
		
		W = quantity.Discrete.eye(dim.d, tauRange);
		w = signals.fir.midpoint( W, tauRange );
		L2filteredDisturbance = signals.fir.mFilter(w, simT.dBounded.^2);
		study.l2Bounds =  max(L2filteredDisturbance);

		mueT = signals.PolyBump.empty;
		for i = 1:dim.y
			mueT(i,1) = signals.PolyBump(tauRange, "a", flat.kappa(i), "b", flat.kappa(i), "norm", true, "numDiff", flat.kappa(i));
		end

		% solve the kernel equations with a H_2^2 optimal performance index:
		% Note: due to numerical problems the number of basis functions must be reduced. Sinusoidal
		% basis function seem to behave better, but for sake of brevity they are not considered in
		% the paper.
		[l2T(k).kernel, l2T(k).args] = computeFaultDetectionKernel(tauRange, systemMatrices, ...
			dim, flat.diffParam, "nu", 9, "basis", "polynomial", "mue0", mueT);	
			
		plotKernel( l2T(k).kernel, l2T(k).args.phi, "figureID", 6);	
		titleText = sprintf( "$L_2$-Norm Permfomance Index \n T = %i, $\\nu$ = %i and $J^* = \\frac{||m_d^T W_d ||_2^2}{||m_f^T W_f ||_2^2}$ = %g " , ...
			Trange(k), l2T(k).args.nu+1, l2T(k).args.J);
		suptitle( titleText );
		drawnow();

		l2T(k).fir = computeFirFilter(tauRange, l2T(k).kernel, ...
		"bounds", study.l2Bounds, "thresholdType", "l2");

		delay.t = quantity.EquidistantDomain( "t", 0, Trange(k)*2, "stepSize", tauRange.stepSize );
		delay.h = quantity.Discrete.eye( 2, delay.t );
		delay.response = abs( signals.fir.mFilter( l2T(k).fir.Mf, delay.h(:,1) ) ) + ...
			abs( signals.fir.mFilter( l2T(k).fir.Mf, delay.h(:,2) ) );

		deltaidx = find( on(abs( delay.response)) >= l2T(k).fir.threshold, 1, "first" );
		l2T(k).delayTime = delay.t.grid( deltaidx );
		fprintf("    Delay time = %g \n", l2T(k).delayTime);
			
		simT.y = sim.y.subs("t", study.t);
		simT.u = sim.u.subs("t", study.t);
		simT.f = sim.f.subs("t", study.t);
		
		firFaultDetection(l2T(k).fir, simT.y, simT.u, "f", simT.f, "plotFault", false, ...
			"d_bounded", simT.dBounded, ...
			"figId", 7, "occurrence", occurrences, "plotThresholds", true);
		h = suptitle( sprintf( "$L_2$-Norm Permfomance Index \n $ T = %g, \\nu$ = %i and $J^* = \\frac{||m_d^T W_d ||_2^2}{||m_f^T W_f ||_2^2} = %g $" , ...
			Trange(k), l2T(k).args.nu+1, l2T(k).args.J));
		h.Interpreter = "latex";
	end

	args = [l2T.args];
	Jrange = [args.J];
	fir = [l2T.fir];
	L2range = [fir.threshold];
	delayTime = [l2T.delayTime];
	% 
	figure(8);clf;
	subplot(311);
	plot(Trange, Jrange);
	xlabel("$T$", "Interpreter", "latex");
	ylabel("$J^*$", "Interpreter", "latex");
	subplot(312); hold on
	% plot(Trange, [fir.thresholdL1]);
	plot(Trange, L2range);
	xlabel("$T$", "Interpreter", "latex");
	ylabel("$\delta_{th}$", "Interpreter", "latex");
	
	subplot(313); hold on;
	plot(Trange, delayTime);
	suptitle("Performance index and threshold in dependence on T");
end

%% 7. export
if flag.export
	% export the fault detection results for the paper:

	exportOptions = {"basepath", ".", "foldername", "data"}; %#ok<*CLARRSTR>

	parameters.nui = flat.args.nu+ 1;
	parameters.T = T;
	parameters.J = flat.args.J;
	parameters.th = flat.fir.threshold;
	parameters.Nt = tau.n;
	parameters.dt = tau.stepSize;
	parameters.tD1 = flat.data.parameters.detection1;
	parameters.tD2 = flat.data.parameters.detection2;
	parameters.tD3 = flat.data.parameters.detection3;

	parameters.fD1 = flat.r.at( parameters.tD1 );
	parameters.fD2 = flat.r.at( parameters.tD2 );
	parameters.fD3 = flat.r.at( parameters.tD3 );

	parameters.t1 = occurrences(1);
	parameters.t2 = occurrences(2);
	parameters.t3 = occurrences(3);
	parameters.t4 = occurrences(4);
	parameters.t1_ = disappears(1);
	parameters.t2_ = disappears(2);
	parameters.t3_ = disappears(3);
	parameters.t4_ = disappears(4);

	parameters.f1 = fI_II(1).max;
	parameters.f2 = fI_II(2).max;
	parameters.fVar = f2(1).fmax;

	parameters.delta1 = inputs.d.l2Bounds(1);
	parameters.delta2 = inputs.d.l2Bounds(2);
	parameters.delta3 = inputs.d.l2Bounds(3);

	data.parameters = export.namevalues(...
			'vars', fieldnames(parameters), ...
			'values', struct2array(parameters), ...
			"filename", "parameters", exportOptions{:});

	data.tdata = export.dd( "M", [Trange', Jrange', L2range', delayTime'], ...
							"header", ["T", "J", "L2", "Deltat"], ...
							"filename", "performanceIndex", exportOptions{:});


	data.detection1 = exportData([sim.f; flat.r; flat.data.boundLower; flat.data.boundUpper], ...
		"header", {"t", "f1", "f2", "r", "thL", "thU"}, ...
		"N", 2001, ...
		"filename", "detection", exportOptions{:});

	data.detectionObs = exportData( r2, "header", {"t", "r1", "r2"}, "filename", "detectionObs", ...
		exportOptions{:});

	data.rms = exportData( [ flat.avR, obs.avR1, obs.avR2 ], "header", {"t", "rms1", "rmsO1", "rmsO2"}, ...
		"filename", "rms", exportOptions{:});

	export.Data.exportAll(data)
end
