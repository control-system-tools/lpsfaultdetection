function plotKernel(kernel, phi, optArgs)
% PLOTKERNEL plot the kernel corresponding to the reference trajectory phi
arguments
	kernel,
	phi
	optArgs.title = ""
	optArgs.figureID = 0;
	optArgs.nOmega = 1e3;
end

	if optArgs.figureID > 0
		figure(optArgs.figureID), clf;
	else
		figure();
	end
	
	plotOptions = {"figureId", 0, "hold", true, "smash", true};
	
	subplot(2,3,1)
	plot( kernel.M, plotOptions{:});

	subplot(2,3,2), 
	plot( kernel.N, plotOptions{:});

	subplot(2,3,3),
	plot( [phi.fun], plotOptions{:});

	subplot(2,3,6),
	% compute the fourier transform
	% maximal frequency: fMax = 1/ ( 2 * dt ) * 2 * pi
	fMax = pi / misc.stepSize( phi(1).domain.grid ); 
	omega = quantity.Domain("w", logspace(-3, log10(fMax), 5e1));
	
	MF = kernel.Mf.fourierTransform(omega);
	semilogx( omega.grid, mag2db( abs( MF.on() ) ) )
	xlabel("$\omega$")
	ylabel("$|m_f|_{dB}$")

	subplot(2,3,4);
	plot( kernel.Mf, plotOptions{:});

	subplot(2,3,5);
	plot( kernel.Md, plotOptions{:});

end